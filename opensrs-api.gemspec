# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'opensrs/api/version'

Gem::Specification.new do |spec|
	spec.name          = 'opensrs-api'
	spec.version       = OpenSRS::API::VERSION
	spec.authors       = ['AP Fritts']
	spec.email         = ['ap@apfritts.com']
	spec.description   = %q{This gem extends the OpenSRS gem by voxxit with classes for (most of) the api for the OpenSRS API.}
	spec.summary       = %q{This gem extends the OpenSRS gem by voxxit with classes for (most of) the api for the OpenSRS API.}
	spec.homepage      = 'https://github.com/apfritts/opensrs-api'
	spec.license       = 'MIT'

	spec.files         = `git ls-files`.split($/)
	spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
	spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
	spec.require_paths = ['lib']

	spec.add_development_dependency 'bundler', '~> 1.3'
	spec.add_development_dependency 'rake'
	spec.add_dependency 'opensrs'
	spec.add_dependency 'activemodel'
end
