require 'bundler/gem_tasks'
require 'csv'
require 'yaml'

desc 'Parse a CSV file with prices and write YAML files'
task :import_csv, :from_file do |t, args|
	headers = [
		'gTLD',
		'New',
		'Renew',
		'Redemption'
	]
	CSV.foreach args[:from_file], headers: headers do |row|
		raw_tld = row['gTLD']
		raw_cost = row['New']

		tld = raw_tld.match(/^\.?([a-zA-Z]+).*$/)[1].to_s.downcase
		cost = raw_cost.match(/(\d+(\.\d{2})?)/)
		next if cost.nil?
		cost = cost.to_s

		update_yaml_file(tld, cost, nil)
	end
end

desc 'Parse a CSV file with gTLD,Sunrise,Registration,Total headers'
task :import_costs, :from_file do |t, args|
	headers = [
		'gTLD',
		'Sunrise',
		'Registration',
		'Total'
	]
	CSV.foreach args[:from_file], headers: headers do |row|
		raw_tld = row['gTLD']
		raw_cost = row['Registration']

		raw_tld_split = raw_tld.match(/^\.?([a-zA-Z]+)\W+(\w+).*$/)
		if raw_tld_split.nil?
			puts "SKIPPING #{raw_tld}"
			next
		end
		tld = raw_tld_split[1].to_s.downcase
		raw_availability = raw_tld_split[2].to_s.downcase
		case raw_availability
		when 'general'
			availability = ''
		when 'landrush'
			availability = 'landrush'
		when 'sunrise'
			availability = 'sunrise'
		else
			puts "Unknown availability status: #{raw_availability}"
			puts row
			availability = 'unknown'
		end
		cost = raw_cost.match(/(\d+(\.\d{2})?)/)
		next if cost.nil?
		cost = cost.to_s

		update_yaml_file(tld, cost, availability)
	end
end

def update_yaml_file(tld, cost, availability)
	# Load the YAML file
	file_name = File.join(File.dirname(__FILE__), 'data', 'domains', "#{tld}.yaml").to_s
	if File.exists? file_name
		data = YAML.load_file file_name
	else
		data = {}
	end

	# Make the data
	data['domain'] = {} unless data.has_key? 'domain'
	data['domain']['cost'] = cost
	if availability.nil?
		unless data['domain'].has_key? 'availability'
			data['domain']['availability'] = 'unknown'
		end
	else
		if availability == ''
			data['domain'].delete 'availability'
		else
			data['domain']['availability'] = availability
		end
	end

	# Write the file
	IO.write(file_name, data.to_yaml)
end
