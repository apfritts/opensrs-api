# Require Voxxit's OpenSRS gem
require 'opensrs'

# Load OpenSRS::API
require 'opensrs/api/version'
require 'opensrs/api/helpers'
require 'opensrs/api/incomplete_profile_error'
require 'opensrs/api/data'
require 'opensrs/api/generic_response'
require 'opensrs/api/lookup'
require 'opensrs/api/models'
require 'opensrs/api/models/domain'
require 'opensrs/api/provision'
require 'opensrs/api/provision/dns'
require 'opensrs/api/provision/domain'
require 'opensrs/api/provision/forwarding'
require 'opensrs/api/provision/nameserver'
require 'opensrs/api/provision/transfer'
require 'opensrs/api/provision/trust'
require 'opensrs/api/validator'
require 'opensrs/api/transfer'
require 'opensrs/api/validator/domain_or_ip_validator'
require 'opensrs/api/validator/domain_validator'
require 'opensrs/api/validator/email_validator'
require 'opensrs/api/validator/ipv4_validator'
require 'opensrs/api/validator/ipv6_validator'
require 'opensrs/api/validator/subdomain_validator'
require 'opensrs/api/validator/phone_validator'

module OpenSRS
	module API
		def self.configure(options)
			defaults = {
				server: @urls[Rails.env == 'production' ? :production : :testing]
			}
			@configuration = defaults.merge(options)
		end

		def self.get_data_path
			@data_path ||= File.join(File.dirname(__FILE__), '..', '..', 'data').to_s
		end

		def self.get_server
			@server ||= OpenSRS::Server.new @configuration
		end

		def self.request(action, object, attrs, mod=nil)
			response = self.get_server.call(
				action:     action,
				object:     object,
				attributes: attrs
			)
			response.extend OpenSRS::API::GenericResponse
			response.extend mod if mod.present?
			response
		end

		def self.paginate(action, object, attrs, attr)
			page = 0
			pages = nil
			results = []
			until page == pages
				page += 1
				attrs[:page] = page
				response = self.request(action, object, attrs)
				if pages.nil?
					page_size = response.get_attribute('page_size')
					total = response.get_attribute('total')
					if /^\d+$/ =~ page_size and /^\d+$/ =~ total
						pages = (total.to_f / page_size.to_f).ceil
						if pages == 0
							return []
						end
					else
						raise PaginateError, 'An invalid page_size and total were returned.'
					end
				end
				attr_response = response.get_attribute(attr)
				if attr_response.present?
					results += attr_response
				end
			end
			results
		end

		def self.get_server_url(type)
			case type.to_sym
			when :testing
				@urls[:testing]
			when :production
				@urls[:production]
			else
				nil
			end
		end

		class PaginateError < StandardError
		end

		private
			@server = nil
			@configuration = nil
			@urls = {
				testing:    'https://horizon.opensrs.net:55443',
				production: 'https://rr-n1-tor.opensrs.net:55443/'
			}
	end
end
