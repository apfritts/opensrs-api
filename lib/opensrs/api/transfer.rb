module OpenSRS::API::Transfer
	def self.check(domain)
		attrs = {
			check_status: true,
			domain: domain,
			get_request_address: true
		}
		r = OpenSRS::API.request('check_transfer', 'domain', attrs)
		r.extend CheckResponse
		r
	end

	module CheckResponse
		include OpenSRS::API::Helpers
		define_attributes [:noservice, :reason, :request_address, :status, :timestamp, :transferrable, :type, :unixtime]
	end

	def self.get_transfers_in(profile)
		profile = GetTransfersInProfile.new if profile.nil?
		r = OpenSRS::API.request('get_transfers_in', 'domain', profile.as_json)
		r.extend GetTransfersInResponse
		r
	end
	class GetTransfersInProfile
		include ActiveModel::Model
		include ActiveModel::Serialization
		attr_accessor :completed_from, :completed_to, :domain, :limit, :losing_registrar, :order_id,
			:order_from, :order_to, :owner_confirm_from, :owner_confirm_ip, :owner_confirm_to,
			:owner_request_from, :owner_request_to, :page, :request_address, :status, :transfer_id
		validates :status, inclusion: { in: ['pending_admin','pending_owner','pending_registry','completed','cancelled'] }
	end
	module GetTransfersInRegister
		include OpenSRS::API::Helpers
		define_attributes [:page, :page_size, :total, :transfers]
		# Transfers contains:
		#   :affiliate_id, :completed_date, :completed_date_epoch, :domain, :gaining_registrar, :losing_registrar, :order_date, :order_date_epoch
		#   :owner_choice, :owner_confirm_date, :owner_confirm_date_epoch, :owner_confirm_ip, :owner_request_date, :owner_request_date_epoch,
		#   :registry_request_date, :registry_request_date_eopch, :request_address, :request_date, :request_date_epoch, :status, :transfer_id
	end
end
