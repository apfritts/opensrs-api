module OpenSRS::API::Lookup
	def self.lookup(domain)
		attrs = {
			domain: domain
		}
		response = OpenSRS::API.request(:lookup, :domain, attrs)
		response.extend LookupResponse
	end

	# TODO: This needs to be much more complete
	def self.get(domain, info = :all_info)
		attrs = { domain: domain, info: info }
		response = OpenSRS::API.request :get, :domain, attrs
		response.extend GetResponse
		response
	end

	module LookupResponse
		include OpenSRS::API::Helpers
		define_attributes [:email_available, :noservice, :price_status, :status]
		def available?
			self.success? and
				self.response['attributes'].present? and self.response['attributes']['status'].present? and self.response['attributes']['status'] == 'available'
		end
	end

	module GetResponse
		include OpenSRS::API::Helpers
		define_attributes [
			# type = admin, billing, owner, or tech
			#:contact_set, :descr
			# type = all_info
			:affiliate_id, :descr, :dns_errors, :nameserver_list, :registry_createdate, :registry_expiredate, :registry_transferdate, :registry_updatedate, :sponsoring_rsp, :tld_data
		]
	end
end
