class OpenSRS::API::Models::Domain < OpenSRS::API::Models
	attr_accessor :domain, :status, :price, :has_claim, :vendor

	def tld
		@tld ||= get_tld
	end

	private
		def get_tld
			self.domain.match(/\.(\w+)$/)[1].to_s
		end
end
