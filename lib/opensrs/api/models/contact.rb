class OpenSRS::API::Models::Contact < OpenSRS::API::Models
	belongs_to :user

	strip_attributes
	validates :first_name, :last_name, :org_name, :city, presence: true, length: { minimum: 1, maximum: 64 }
	validates :address1, presence: true, length: { minimum: 1, maximum: 100 }
	validates :address2, length: { maximum: 100 }
	validates :postal_code, presence: true, length: { minimum: 1, maximum: 16 }
	validates :state, :country, :lang, length: { is: 2 }
	validates :phone, presence: true, phone: true, length: { maximum: 20 }
	validates :email, presence: true, email: true, length: { maximum: 255 }
end
