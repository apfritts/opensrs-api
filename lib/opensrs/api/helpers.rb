module OpenSRS::API::Helpers
	module ClassMethods
		def define_attributes(attrs = [])
			attrs.each do |attr|
				define_method(attr.to_s) do
					if self.response.present? and self.response['attributes'].present?
						self.response['attributes']["#{attr.to_s}"]
					end
				end
				define_method("#{attr.to_s}!") do
					if self.response.present? and self.response['attributes'].present? and self.response['attributes']["#{attr.to_s}"].present?
						self.response['attributes']["#{attr.to_s}"]
					else
						raise NoAttributeError, "Unknown attribute #{attr.to_s}"
					end
				end
			end
		end
	end

	class NoAttributeError < StandardError
	end

	def self.included(klass)
		klass.extend ClassMethods
	end
end
