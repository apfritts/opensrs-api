module OpenSRS::API::Constants
	include OpenSRS::API::Constants::TLDs
	WHOIS_PRIVACY_STATE = [:enabled, :disabled, :enabling, :disabling]
end
