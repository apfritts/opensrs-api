module OpenSRS::Contstants::TLDs
	TLDs = {
		ggTLD: {
			AERO: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			ASIA: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			BIZ: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			COOP: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			COM: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			INFO: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			JOBS: {
				contacts: [:owner]
			},
			MOBI: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			NAME: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			NET: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			ORG: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			PRO: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			TEL: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			XXX: {
				contacts: [:owner, :admin, :billing, :tech]
			}
		},

		ccTLD: {
			AC: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			AE: {
				contacts: [:owner, :admin, :tech]
			},
			AF: {
				contacts: [:owner, :admin, :billing, :tech]
			},
			'COM.AF' => :AF,
			'NET.AG' => :AF,
			'ORG.AF' => :AF
			# TODO: Complete TLD listing here
		}
	}

	private
		def default_fields
			{
				first_name: { presence: true, length: { maximum: 64  } },
				last_name:  { presence: true, length: { maximum: 64  } },
				org_name:   { presence: true, length: { maximum: 100 } }
			}
		end
end
