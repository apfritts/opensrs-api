module OpenSRS::API
	class IncompleteProfileError < StandardError
		attr_reader :profile
		def initialize(profile)
			@profile = profile
		end
	end
end
