class OpenSRS::API::Validator::PhoneValidator < OpenSRS::API::Validator
	def validate_each(record, attribute, value)
		unless value =~ REGEX_MATCH
			record.errors[attribute] << (options[:message] || 'is not a proper phone number')
		end
	end

	REGEX_MATCH = /\A\+([0-9]{1,3})\.([0-9]{4,14})(?:x(.+))?\z/i
	REGEX_PARTIAL = /\A(?:\+([0-9]{1,3})\.)?([0-9]{4,14})?(?:x(.+))?\z/i
end
