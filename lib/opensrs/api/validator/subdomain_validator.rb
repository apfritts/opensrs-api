class OpenSRS::API::Validator::SubdomainValidator < OpenSRS::API::Validator
	def validate_each(record, attribute, value)
		unless (value == '' or value =~ SubdomainValidator::VALID_SUBDOMAIN_NAME)
			record.errors[attribute] << (options[:message] or 'is not a proper subdomain name')
		end
	end

	#VALID_SUBDOMAIN_NAME = /\A((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]))(\.[A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])*)|\*\z/i
	VALID_SUBDOMAIN_NAME = /\A((([a-zA-Z0-9_]|[a-zA-Z0-9_][a-zA-Z0-9\-_]*[a-zA-Z0-9_]))(\.[A-Za-z0-9_]|[A-Za-z0-9_][A-Za-z0-9\-_]*[A-Za-z0-9_])*)|\*\z/i
end
