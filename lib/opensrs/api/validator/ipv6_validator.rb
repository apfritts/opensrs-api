class OpenSRS::API::Validator::IPv6Validator < OpenSRS::API::Validator
	def validate_each(record, attribute, value)
		unless value =~ VALID_IPv6_ADDRESS
			record.errors[attribute] << (options[:message] || 'is not a proper IPv6 address')
		end
	end

	VALID_IPv6_ADDRESS = Resolv::IPv6::Regex
end
