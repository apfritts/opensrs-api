class OpenSRS::API::Validator::DomainOrIpValidator < OpenSRS::API::Validator
	def validate_each(record, attribute, value)
		unless value =~ DomainOrIpValidator::VALID_IP_ADDRESS or value =~ DomainValidator::VALID_HOST_NAME
			record.errors[attribute] << (options[:message] || 'is not a proper domain name or IP address')
		end
	end

	# TODO: Support IPv6
	VALID_IP_ADDRESS = /\A(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\z/i
end
