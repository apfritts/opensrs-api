class OpenSRS::API::Validator::DomainValidator < OpenSRS::API::Validator
	def validate_each(record, attribute, value)
		unless value =~ DomainValidator::VALID_HOST_NAME
			record.errors[attribute] << (options[:message] || 'is not a proper domain name')
		end
	end

	VALID_HOST_NAME = /\A(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)+([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])\z/i
end
