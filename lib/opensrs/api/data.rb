require 'yaml'

module OpenSRS::API::Data
	# TODO: Caching should be on the public functions, not the private one
	def self.get_domain_defaults
		load_yaml :domains, 'defaults'
	end

	def self.get_domain(name)
		custom = load_yaml :domains, name
		defaults = get_domain_defaults
		defaults.deep_merge custom
	end

	def self.get_all_domains
		files = Dir[File.join(OpenSRS::API.get_data_path, 'domains', '*.yaml')]
		data = Hash.new
		files.each do |f|
			key = File.basename(f).match(/(\w+)\./)[1].to_s
			next if key == 'defaults'
			data.store key, get_domain(key)
		end
		data
	end

	private
		def self.load_yaml(type, file)
			return @files[type][file] unless @files[type][file].nil?
			path = File.join(OpenSRS::API.get_data_path, type.to_s, "#{file}.yaml").to_s
			if File.exists? path
				yaml = IO.read path
				data = YAML.load yaml
				@files[type][file] = data
				data
			else
				{}
			end
		end

		@files = {domains: {}, trust: {}}
end
