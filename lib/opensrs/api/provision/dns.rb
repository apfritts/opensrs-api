module OpenSRS::API::Provision::DNS
	def self.force_nameservers(domain)
		attrs = { domain: domain }
		OpenSRS::API.request :force_dns_nameservers, :domain, attrs
	end

	def self.create_zone(profile)
		raise OpenSRS::API::IncompleteProfileError, profile unless profile.valid?
		attrs = profile.as_json.reject{|k,v| ['validation_context','errors'].include? k}
		response = OpenSRS::API.request :create_dns_zone, :domain, attrs
		response.extend CreateZoneResponse
		response
	end

	def self.get_zone(domain)
		attrs = { domain: domain }
		response = OpenSRS::API.request :get_dns_zone, :domain, attrs
		response.extend GetZoneResponse
		response
	end

	def self.delete_zone(domain)
		attrs = { domain: domain }
		OpenSRS::API.request :delete_dns_zone, :domain, attrs
	end

	def self.set_zone(profile)
		raise OpenSRS::API::IncompleteProfileError, profile unless profile.valid?
		attrs = profile.as_json.reject{|k,v| ['validation_context','errors'].include? k}
		response = OpenSRS::API.request :set_dns_zone, :domain, attrs
		response.extend SetZoneResponse
		response
	end

	def self.create_or_set_zone(profile)
		raise OpenSRS::API::IncompleteProfileError, profile unless profile.valid?
		zone = get_zone(profile.domain)
		if zone.zone_exists?
			set_zone(profile)
		else
			create_zone(profile)
		end
	end

	class CreateZoneProfile
		include ActiveModel::Model
		include ActiveModel::Serialization
		attr_accessor :domain, :records
		validate :domain, :records, presence: true
		# TODO: Validate records is an associative array of arrays of hashes for each record type: :A, :AAAA, :CNAME, :MX, :SRV, :TXT
	end

	class SetZoneProfile < CreateZoneProfile
	end

	module ZoneResponse
		include OpenSRS::API::Helpers
		define_attributes [:nameservers_ok, :records]

		def zone_exists?
			if /zone not found for domain/ =~ self.response['response_text']
				false
			else
				true
			end
		end
	end

	module CreateZoneResponse
		include ZoneResponse
	end

	module GetZoneResponse
		include ZoneResponse
	end

	module SetZoneResponse
		include ZoneResponse
	end
end
