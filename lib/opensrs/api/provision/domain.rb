module OpenSRS::API::Provision::Domain
	def self.check(domain, tlds = ['.com', '.net', '.org'], services=['lookup','suggestion'])
		attrs = {
			searchstring: domain,
			tlds: tlds,
			services: services
		}
		response = OpenSRS::API.request :name_suggest, :domain, attrs, CheckResponse

		lookup = response.get_attribute('lookup')
		suggest = response.get_attribute('suggestion')
		if lookup and lookup['items']
			response.domains = lookup['items'].map{|d| OpenSRS::API::Models::Domain.new d}
		end
		if suggest and suggest['items']
			response.suggestions = suggest['items'].map{|d| OpenSRS::API::Models::Domain.new d}
		end
		response
	end

	def self.register(profile)
		raise OpenSRS::API::IncompleteProfileError, profile unless profile.valid?
		attrs = profile.as_json.reject{|k,v| ['validation_context','errors'].include? k}
		OpenSRS::API.request :sw_register, :domain, attrs
	end

	def self.modify(profile)
		raise OpenSRS::API::IncompleteProfileError, profile unless profile.valid?
		attrs = profile.as_json.reject{|k,v| ['validation_context','errors'].include? k}
		OpenSRS::API.request :modify, :domain, attrs, ModifyResponse
	end

	def self.update_contacts(profile)
		raise OpenSRS::API::IncompleteProfileError, profile unless profile.valid?
		attrs = profile.as_json.reject{|k,v| ['validation_context','errors'].include? k}
		OpenSRS::API.request :update_contacts, :domain, attrs, UpdateContactsResponse
	end

	def self.get(domain)
		OpenSRS::API.request :get, :domain, { domain: domain, type: :all_info }, GetResponse
	end

	def self.get_privacy_status(domain)
		OpenSRS::API.request :get, :domain, {domain: domain, type: :whois_privacy_state}, GetPrivacyStatusResponse
	end

	def self.get_lock_status(domain)
		OpenSRS::API.request :get, :domain, {domain: domain, type: :status}, GetLockStatusResponse
	end

	def self.get_auth_info(domain)
		OpenSRS::API.request :get, :domain, {domain: domain, type: :domain_auth_info}, GetAuthInfoResponse
	end

	def self.get_registrant_verification_status(domain)
		OpenSRS::API.request :get_registrant_verification_status, :domain, {domain: domain}, GetRegistrantVerificationStatus
	end

	def self.get_expiring_domains(exp_from, exp_to, limit, page)
		raise NotImplementedError
	end

	module CheckResponse
		include OpenSRS::API::Helpers
		attr_accessor :domains, :suggestions
	end

	class RegisterProfile
		include ActiveModel::Model
		include ActiveModel::Serialization
		attr_accessor :affiliate_id, :auto_renew, :change_contact, :comments, :contact_set, :custom_nameservers, :custom_transfer_nameservers,
			:custom_tech_contact, :dns_template, :domain, :encoding_type, :f_lock_domain, :f_parkp, :f_whois_privacy, :handle, :link_domains, :master_order_id,
			:nameserver_list, :owner_confirm_address, :period, :premium_price_to_verify, :reg_domain, :reg_username, :reg_password, :reg_type, :tld_data,
			:trademark_smd

		validates :contact_set, :custom_tech_contact, :domain, :period, presence: true
		validates :custom_nameservers, inclusion: { in: ['0', '1', 0, 1, false, true] }
		validates :handle, presence: true, inclusion: { in: [:save, :process] }
		validates :master_order_id, presence: true, if: 'link_domains.to_s == "1"'
	end

	module RegisterResponse
		include OpenSRS::API::Helpers
		define_attributes [:admin_email, :cancelled_orders, :error, :forced_pending, :id, :queue_request_id, :registration_code, :registration_text, :transfer_id, :whois_privacy_state]
	end

	class ModifyProfile
		include ActiveModel::Model
		include ActiveModel::Serialization

		attr_accessor :affect_domains, :data, :domain, :contact_set, :org_name, :domain_auth_info, :auto_renew, :let_expire, :lock_state, :state

		validates :affect_domains, inclusion: { in: [false, 0, '0'] }  # TODO: Allow value to be true or 1 (which requires more obnoxious validation below)
		validates :data, presence: true, inclusion: { in: ['contact_info', 'domain_auth_info', 'expire_action', 'status', 'whois_privacy_state', :contact_info, :domain_auth_info, :expire_action, :status, :whois_privacy_state] }
		validates :domain, presence: true

		validates :contact_set, :org_name, presence: true, if: 'data.to_s == "contact_info"'

		validates :domain_auth_info, presence: true, if: 'data.to_s == "domain_auth_info"'

		validates :auto_renew, :let_expire, inclusion: { in: [false, true, 0, 1, '0', '1'] }, if: 'data.to_s == "expire_action"'

		validates :lock_state, inclusion: { in: [false, true, 0, 1, '0', '1'] }, if: 'data.to_s == "status"'

		validates :state, presence: true, inclusion: { in: ['enable', 'disable'] }, if: 'data.to_s == "whois_privacy_state"'
	end

	module ModifyResponse
		include OpenSRS::API::Helpers
		define_attributes [:details, :waiting_requests_no]
	end

	class UpdateContactsProfile
		include ActiveModel::Model
		include ActiveModel::Serialization
		attr_accessor :affect_domains, :contact_set, :domain, :report_email, :types
		validate :contact_set, :domain, :types, presence: true
	end

	module UpdateContactsResponse
		include OpenSRS::API::Helpers
		define_attributes [:details, :encoding_type, :waiting_requests_no]
	end

	module GetResponse
		include OpenSRS::API::Helpers
		define_attributes [:affiliate_id, :auto_renew, :descr, :dns_errors, :nameserver_list, :registry_createdate, :registry_expiredate, :registry_transferdate, :registry_updatedate, :sponsoring_rsp, :tld_data]
		def auto_renew?
			self.auto_renew == '1'
		end
	end

	module GetPrivacyStatusResponse
		include OpenSRS::API::Helpers
		define_attributes [:changeable, :state]
		def enabled?
			self.state == 'enabled'
		end
		def modifiable?
			self.changeable == '1'
		end
	end

	module GetLockStatusResponse
		include OpenSRS::API::Helpers
		define_attributes [:auctionescrow, :can_modify, :domain_supports, :lock_state, :parkp_status, :transfer_away_in_progress]
		def enabled?
			self.lock_state == '1'
		end
		def modifiable?
			self.can_modify == '1'
		end
	end

	module GetAuthInfoResponse
		include OpenSRS::API::Helpers
		define_attributes [:domain_auth_info]
	end

	module GetRegistrantVerificationStatus
		include OpenSRS::API::Helpers
		define_attributes [:days_to_suspend, :email_bounced, :registrant_verification_status, :verification_deadline]
	end
end

