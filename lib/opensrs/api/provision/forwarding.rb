module OpenSRS::API::Provision::Forwarding
	def self.get_config(domain)
		attrs = { domain: domain }
		response = OpenSRS::API.request :get_domain_forwarding, :domain, attrs
		response.extend GetConfigResponse
		response
	end

	def self.create(domain)
		attrs = { domain: domain }
		OpenSRS::API.request :create_domain_forwarding, :domain, attrs
	end

	def self.configure(profile)
		raise InvalidProfileError, profile unless profile.valid?
		attrs = profile.as_json.reject{|k,v| ['validation_context','errors'].include? k}
		OpenSRS::API.request :set_domain_forwarding, :domain, attrs
	end

	def self.remove(domain)
		attrs = { domain: domain }
		OpenSRS::API.request :delete_domain_forwarding, :domain, attrs
	end

	module GetConfigResponse
		include OpenSRS::API::Helpers
		define_attributes [:forwarding]
		def forwarding_disabled?
			not self.success? and self.response['response_text'] == 'Domain does not have the domain forwarding service' ? true : nil
		end
	end

	class ConfigureProfile
		include ActiveModel::Model
		include ActiveModel::Serialization
		attr_accessor :domain, :forwarding
		validate :domain, :forwarding, presence: true
		# TODO: Check the objects in the forwarding array
		# Contents should be: :destination_url (must be valid URL with protocol), :enabled, :masked, :subdomain (max characters is 128)
		# Contents should be: :keywords (max characters is 900), :title (max characters is 255)
	end
end
