module OpenSRS::API::Provision::Nameserver
	def self.list(domain)
		attrs = { domain: domain, type: :nameservers }
		response = OpenSRS::API.request(:get, :domain, attrs)
		response.extend ListResponse
		response
	end

	def self.assign(profile)
		raise OpenSRS::API::IncompleteProfileError, profile unless profile.valid?
		attrs = { domain: profile.domain, assign_ns: profile.nameservers, op_type: :assign }
		OpenSRS::API.request(:advanced_update_nameservers, :domain, attrs)
	end

	def self.remove(profile)
		raise OpenSRS::API::IncompleteProfileError, profile unless profile.valid?
		attrs = { domain: profile.domain, remove_ns: profile.nameservers, op_type: :add_remove }
		OpenSRS::API.request(:advanced_update_nameservers, :domain, attrs)
	end

	module ListResponse
		include OpenSRS::API::Helpers
		define_attributes [:nameserver_list]
	end

	class AssignProfile
		include ActiveModel::Model
		include ActiveModel::Serialization
		attr_accessor :domain, :nameservers
		validates :domain, presence: true
		validates :nameservers, presence: true
	end

	class RemoveProfile < AssignProfile
	end
end
