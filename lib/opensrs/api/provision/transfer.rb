module OpenSRS::API::Provision::Transfer
	def self.check(domain)
		OpenSRS::API.request :check_transfer, :domain, {check_status: 1, domain: domain, get_request_address: 1}, CheckResponse
	end

	def self.get_incoming(profile=GetIncomingProfile.new)
		#raise OpenSRS::API::IncompleteProfileError, profile unless profile.valid?
		attrs = profile.as_json.reject{|k,v| ['validation_context','errors'].include? k}
		OpenSRS::API.paginate :get_transfers_in, :domain, attrs, :transfers
	end

	def self.get_outgoing(profile=GetOutgoingProfile.new)
		#raise OpenSRS::API::IncompleteProfileError, profile unless profile.valid?
		attrs = profile.as_json.reject{|k,v| ['validation_context','errors'].include? k}
		OpenSRS::API.paginate :get_transfers_away, :domain, attrs, :transfers
	end

	module CheckResponse
		include OpenSRS::API::Helpers
		define_attributes [:noservice, :reason, :request_address, :status, :timestamp, :transferrable, :type, :unixtime]
	end

	class GetIncomingProfile
		include ActiveModel::Model
		include ActiveModel::Serialization
		attr_accessor :completed_from, :completed_to, :status
	end

	class GetOutgoingProfile
		include ActiveModel::Model
		include ActiveModel::Serialization
		attr_accessor :req_from, :req_to, :status
	end
end
