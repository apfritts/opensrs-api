module OpenSRS::API::Provision::General
	def self.get_order(order_id, type)
		attrs = {
			order_id: order_id
		}
		raise ArgumentError, 'Type must be :domain or :trust_service' unless [:domain, :trust_server].include? type
		response = OpenSRS::API.request(:get_order_info, type, attrs)
		response.extend GetOrderDomainResponse if type == :domain
		response.extend GetOrderTrustResponse if type == :trust_service
	end

	class GetOrderDomainResponse
		attr_accessor :affiliate_id, :application_id, :application_status, :comments, :completed_date,
			:cost, :domain, :encoding_type, :expiry_year, :f_auto_renew, :f_lock_domain,
			:flag_saved_ns_fields, :flag_saved_tech_fields, :forwarding_email, :fqdns, :id,
			:master_order_id, :notes, :order_date, :owner_contact, :admin_contact, :billing_contact,
			:tech_contact, :owner_confirm_time, :owner_request_time, :period, :processed_date, :reg_domain,
			:reg_type, :registry_request_time, :request_address, :status, :tld_data, :transfer_notes,
			:transfer_status
	end

	class GetOrderTrustResponse
		attr_accessor :approver_email, :contact_email, :contact_set, :csr, :domain, :notes_list, :order_id,
			:period, :price, :product_id, :product_type, :reg_type, :server_type, :special_instructions,
			:state, :supplier_order_id
	end
end
