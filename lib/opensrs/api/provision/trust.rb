module OpenSRS::API::Provision::Trust
	def register(profile)
		raise OpenSRS::API::IncompleteProfileError, profile unless profile.valid?
		response = OpenSRS::API.request(:sw_register, :trust_service, attrs)
		response.extend TrustResponse
		response
	end

	class TrustProfile
		include ActiveModel::Model
		include ActiveModel::Validations::Callbacks
		attr_accessor :additional_domains, :approver_email, :base_order_id,
			:contact_set, :csr, :domain, :end_user_auth_info, :handle,
			:inventory_item_id, :order_id, :period, :product_id, :product_type,
			:reg_type, :search_in_seal, :server_count, :server_type,
			:special_instructions, :trust_seal, :email_address, :password, :username

		validates :contact_set, :domain, :handle, :period, presence: true
		validates :product_type, presence: true, inclusion: { in: OpenSRS::API::Provision::PRODUCT_TYPE.keys }
		validates :reg_type, presence: true, inclusion: { in: OpenSRS::API::Provision::REG_TYPE }

		before_validation do
			self.product_type = product_type.to_sym unless product_type.nil?
			self.reg_type = reg_type.to_sym unless reg_type.nil?
		end
	end

	module TrustResponse
		attr_accessor :domain, :order_id, :state
	end
end

