module OpenSRS::API::GenericResponse
	def get_attribute(attr)
		if self.response.present? and self.response['attributes'].present?
			self.response['attributes']["#{attr.to_s}"]
		end
	end
end
