# OpenSRS::API

[(https://codeclimate.com/github/apfritts/opensrs.png)](https://codeclimate.com/github/apfritts/opensrs)

This gem extends the [OpenSRS gem by voxxit](https://github.com/voxxit/opensrs) with classes for (most of) the api for the [OpenSRS API](http://www.opensrs.com/site/resources/documentation/api). This includes:

* Computer-readable feature and requirement tables
* Domain, SSL Certficate, and Contact Models
* Additional validation classes

## Installation

Add this line to your application's Gemfile:

	gem 'opensrs-api'

And then execute:

	$ bundle

Or install it yourself as:

	$ gem install opensrs-api

## Usage

### Getting setup
Set your user name, password, and key in an initializer:

	OpenSRS::API::configure {
		username: 'blahblah',
		password: 'supersecret',
		key: 'mayahimayahimayahi'
	}

Make sure you add your IP address(es) to the OpenSRS whitelist under Profile Management > Add IP for Script/API Access. Wait for the changes to take effect.

The OpenSRS gem supports LibXML and Nokogiri as XML parsers. By default, it uses LibXML to parse and generate XML. If you'd like to use Nokogiri (1.4.7 and below), add this to your initializer:

	OpenSRS::Server.xml_processor = :nokogiri

### How services are organized

Services are broken out in modules, with class methods to do processing. Class methods may be called directly with a few arguments, but for things like domain registration, this would be obnoxious. So there are profiles, which specify (and have validators!) to make sure the information provided will (more likely) be executed properly. Each method will also have a response object based off `OpenSRS::API::GenericResponse`.

Here's an example of registering a Trust Certificate:

	cert = OpenSRS::API::Provision::Trust::RegisterProfile.new
	cert.
	cert.valid? # returns true or false
	cert.errors.any? # returns true or false, just like an ActiveRecord model
	cert.error.full_messages.each do |msg|
		puts msg
	end
	cert.
	begin
		response = OpenSRS::API::Provision::Trust.register(cert)
		response.is_success? # returns true or false
		response.is_a?(OpenSRS::API::GenericResponse)
	rescue OpenSRS::API::IncompleteProfileError => e
		puts "Sorry! Your profile for domain #{e.profile.domain} is not complete!"
	end

## Coding Style

Since it's unique-ish...

* I use tabs to begin lines

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Code like crazy using tabs (vi: `:%s/  /\t/g`)
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to the branch (`git push origin my-new-feature`)
6. Create new Pull Request
